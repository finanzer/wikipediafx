package org.finanzer.wikipedia.editor.model;

public enum MediaType {
    WIKI, CSS, IMAGE
}
