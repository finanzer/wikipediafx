package org.finanzer.wikipedia.editor.editor;

import net.sf.saxon.functions.Abs;
import org.finanzer.wikipedia.editor.editor.regex.CssRegexLexer;
import org.finanzer.wikipedia.editor.editor.regex.RegexToken;
import org.finanzer.wikipedia.editor.editor.regex.WikiRegexLexer;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import org.languagetool.rules.RuleMatch;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class WikiRichTextCodeEditor extends AbstractRichTextCodeEditor {
    private final WikiRegexLexer wikiRegexLexer = new WikiRegexLexer();


    public WikiRichTextCodeEditor()
    {
        super();
        String stylesheet = AbstractRichTextCodeEditor.class.getResource("/editor-css/wiki.css").toExternalForm();
        addStyleSheet(stylesheet);
        setWrapText(true);
    }

    @Override
    protected StyleSpans<Collection<String>> computeHighlighting(String text) {
        StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
        wikiRegexLexer.setContent(text);

        int lastKwEnd = 0;
        RegexToken token;

        while ((token = wikiRegexLexer.nextToken()) != null) {
            spansBuilder.add(Collections.emptyList(), token.getStart() - lastKwEnd);
            spansBuilder.add(Collections.singleton(token.getCode().toLowerCase()), token.getEnd() - token.getStart());
            lastKwEnd = token.getEnd();
        }

        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return spansBuilder.create();
    }

    @Override
    public List<RuleMatch> spellCheck() {
        return null;
    }

    @Override
    public void applySpellCheckResults(List<RuleMatch> matches) {
    }

    public void increaseIndent() {

    }

    public void decreaseIndent() {

    }
}
