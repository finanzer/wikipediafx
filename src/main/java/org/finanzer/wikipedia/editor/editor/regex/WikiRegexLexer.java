package org.finanzer.wikipedia.editor.editor.regex;

/**
 * User: Michail Jungierek
 * Date: 06.07.2019
 * Time: 02:52
 */
public class WikiRegexLexer extends RegexLexer {
    public WikiRegexLexer() {
        super();

        addRule(new RegexKeywordLexerRule("FUNCTION", ""));
        addRule(new RegexKeywordLexerRule("KEYWORD", ""));
        addRule(new RegexKeywordLexerRule("VALUE", ""));

        addRule(new RegexKeywordLexerRule("PROPERTY", ""));


        addRule(new RegexLexerRule("PAREN", "\\(|\\)"));
        addRule(new RegexLexerRule("BRACE", "\\{\\{|\\}\\}"));
        addRule(new RegexLexerRule("BRACKET", "\\[|\\]"));
        addRule(new RegexLexerRule("CONTROL", "\\;|\\,|\\:"));
        addRule(new RegexLexerRule("STRING", "((\"([^\"\\\\]|\\\\.)*\")|(\'([^\'\\\\]|\\\\.)*\'))"));
        addRule(new RegexLexerRule("COMMENT", "//[^\n]*" + "|" + "/\\*(.|\\R)*?\\*/"));

        addRule(new RegexLexerRule("NUMBER", "[0-9]+(\\.[0-9]+)?"));
        addRule(new RegexLexerRule("COLOR", "\\#[\\dA-Fa-f]{2,6}"));

        addRule(new RegexLexerRule("ATTRIBUTE", "[a-zA-Z\\_\\-]{1}[a-zA-Z0-9\\_\\-]{0,}[ ]{0,}\\:", -1));

    }
}
