package org.finanzer.wikipedia.editor.model;

import org.apache.commons.compress.utils.Lists;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class ArticleCollections {
    private List<ArticleCollection> collections;

    public ArticleCollections() {
        collections = new ArrayList<>();
        ArticleCollection baseCollection = new ArticleCollection();
        baseCollection.setName("Base Collection");
        collections.add(baseCollection);
    }

    public List<ArticleCollection> getCollections() {
        return collections;
    }

    public boolean isUnsavedPage() {
        return false;
    }
}
