package org.finanzer.wikipedia.editor.model;

import java.util.ArrayList;
import java.util.List;

public class ArticleSection {
    private String title;
    private int level;
    private List<ArticleSection> children = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ArticleSection> getChildren() {
        return children;
    }

    public boolean hasChildren() {
        return !children.isEmpty();
    }
}
