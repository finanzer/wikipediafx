package org.finanzer.wikipedia.editor.manager;

import java.util.List;

import javax.inject.Singleton;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import org.apache.log4j.Logger;

import org.finanzer.wikipedia.editor.model.ArticleSection;

/**
 * User: mjungierek
 * Date: 27.07.2014
 * Time: 12:35
 */
@Singleton
public class TOCViewManager
{
    private static final Logger logger = Logger.getLogger(TOCViewManager.class);

    private TreeView<ArticleSection> treeView;
    private EditorTabManager editorManager;
    private TreeItem<ArticleSection> rootItem;

    public void setTreeView(TreeView<ArticleSection> treeView)
    {
        this.treeView = treeView;
        rootItem = new TreeItem<>();
        rootItem.setExpanded(true);
        treeView.setRoot(rootItem);
        treeView.setShowRoot(false);

        treeView.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton().equals(MouseButton.PRIMARY))
            {
                if (event.getClickCount() == 2)
                {
                    //goto section
                }
            }
        });

    }

    private void addChildren(TreeItem<ArticleSection> parentItem)
    {
        ArticleSection parent = parentItem.getValue();
        List<ArticleSection> children = parent.getChildren();
        for (ArticleSection child : children)
        {
            TreeItem<ArticleSection> childItem = new TreeItem<>(child);
            parentItem.getChildren().add(childItem);
            if (child.hasChildren())
            {
                addChildren(childItem);
            }
        }
    }

    public void setEditorManager(EditorTabManager editorManager)
    {
        this.editorManager = editorManager;
    }
}
