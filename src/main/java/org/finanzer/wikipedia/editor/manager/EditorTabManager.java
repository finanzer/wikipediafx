package org.finanzer.wikipedia.editor.manager;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableBooleanValue;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.finanzer.wikipedia.editor.BeanFactory;
import org.finanzer.wikipedia.editor.EditorConfiguration;
import org.finanzer.wikipedia.editor.clips.Clip;
import org.finanzer.wikipedia.editor.clips.ClipManager;
import org.finanzer.wikipedia.editor.editor.CodeEditor;
import org.finanzer.wikipedia.editor.editor.CssRichTextCodeEditor;
import org.finanzer.wikipedia.editor.editor.EditorPosition;
import org.finanzer.wikipedia.editor.editor.WikiRichTextCodeEditor;
import org.finanzer.wikipedia.editor.gui.ExceptionDialog;
import org.finanzer.wikipedia.editor.model.Article;
import org.finanzer.wikipedia.editor.model.CssPage;
import org.finanzer.wikipedia.editor.model.ImagePage;
import org.finanzer.wikipedia.editor.model.MediaType;
import org.finanzer.wikipedia.editor.model.Page;
import org.finanzer.wikipedia.editor.preferences.PreferencesManager;
import org.fxmisc.richtext.CodeArea;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import java.io.IOException;
import java.net.URL;
import java.util.Deque;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * User: mjungierek
 * Date: 21.07.2014
 * Time: 20:29
 */
@Singleton
public class EditorTabManager {
    private static final Logger logger = Logger.getLogger(EditorTabManager.class);

    private TabPane tabPane;
    private ObjectProperty<CodeEditor> currentEditor = new SimpleObjectProperty<>();
    //getrennte Verwaltung der current resource für html und css, da der Previewer auf der article property lauscht und
    // wenn ein css bearbeitet wird, der letzte article weiterhin im previewer angezeigt werden soll
    private ReadOnlyObjectWrapper<Page> currentSearchablePage = new ReadOnlyObjectWrapper<>();
    private ReadOnlyObjectWrapper<Article> currentArticle = new ReadOnlyObjectWrapper<>();
    private ReadOnlyObjectWrapper<CssPage> currentCssPage = new ReadOnlyObjectWrapper<>();
    private BooleanProperty needsRefresh = new SimpleBooleanProperty(false);
    private SimpleBooleanProperty currentEditorIsArticle = new SimpleBooleanProperty();
    private SimpleBooleanProperty canUndo = new SimpleBooleanProperty();
    private SimpleBooleanProperty canRedo = new SimpleBooleanProperty();
    private StringProperty cursorPosLabelProperty = new SimpleStringProperty();
    private final ReadOnlyIntegerWrapper currentLineProperty = new ReadOnlyIntegerWrapper(this, "currentLine");

    private ContextMenu contextMenuArticle;
    private ContextMenu contextMenuCSS;

    @Inject
    private ClipManager clipManager;
    @Inject
    private ArticleManager articleManager;
    @Inject
    private PreferencesManager preferencesManager;
    @Inject
    private Provider<WikiRichTextCodeEditor> wikiEditorProvider;
    @Inject
    private Provider<CssRichTextCodeEditor> cssEditorProvider;
    @Inject
    private EditorConfiguration configuration;

    private boolean openingEditorTab = false;
    private boolean refreshAllInProgress = false;
    private boolean refreshAll = false;

    public static class ImageViewerPane extends ScrollPane implements Initializable {
        @FXML
        private ImageView imageView;
        @FXML
        private Label imagePropertiesLabel;
        @FXML
        private VBox vBox;
        private ImagePage imagePage;

        ImageViewerPane() {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/image_view.fxml"), null, new JavaFXBuilderFactory(),
                    type -> BeanFactory.getInstance().getBean(type));
            loader.setRoot(this);
            loader.setController(this);

            try {
                loader.load();
            }
            catch (IOException e) {
                ExceptionDialog.showAndWait(e, null, "Show Image", "Error while opening image.");
                logger.error("", e);
            }
        }

        @Override
        public void initialize(URL location, ResourceBundle resources) {
            vBox.minWidthProperty().bind(this.widthProperty());
            vBox.minHeightProperty().bind(this.heightProperty());
        }

        public Label getImagePropertiesLabel() {
            return imagePropertiesLabel;
        }

        public void setImagePropertiesLabel(Label imagePropertiesLabel) {
            this.imagePropertiesLabel = imagePropertiesLabel;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public void setImageView(ImageView imageView) {
            this.imageView = imageView;
        }

        public ImagePage getImagePage() {
            return imagePage;
        }

        public void setImagePage(ImagePage imagePage) {
            this.imagePage = imagePage;
        }
    }

    @PostConstruct
    public void init() {
        currentEditor.addListener((observable, oldValue, newValue) -> {
            canUndo.unbind();
            canRedo.unbind();
            if (newValue != null) {
                currentEditorIsArticle.setValue(currentEditor.getValue().getMediaType().equals(MediaType.WIKI));
                canUndo.bind(currentEditor.getValue().canUndoProperty());
                canRedo.bind(currentEditor.getValue().canRedoProperty());
            }
            else {
                currentEditorIsArticle.setValue(false);
                canUndo.setValue(false);
                canRedo.setValue(false);
            }
        });

        MenuItem separatorItem = new SeparatorMenuItem();
        //Html menu
        contextMenuArticle = new ContextMenu();
        contextMenuArticle.getStyleClass().add("context-menu");
        contextMenuArticle.setAutoFix(true);
        contextMenuArticle.setAutoHide(true);

        Menu clipsItem = new Menu("Text Snippets");
        clipManager.clipsRootProperty().addListener(event -> {
            logger.info("whole clips tree changed, (re)build the clip menu");
            clipsItem.getItems().clear();
            writeClipMenuItemChildren(clipManager.getClipsRoot(), clipsItem);
        });
        clipManager.setOnClipsTreeChanged(c -> {
            clipsItem.getItems().clear();
            writeClipMenuItemChildren(clipManager.getClipsRoot(), clipsItem);
        });
        contextMenuArticle.getItems().add(clipsItem);
        contextMenuArticle.getItems().add(separatorItem);

        MenuItem openInExternalBrowserItem = new MenuItem("Open in external Browser");
        openInExternalBrowserItem.setOnAction(e -> {
            openInExternalBrowser(currentEditor);
        });
        contextMenuArticle.getItems().add(openInExternalBrowserItem);

        //css menu
        contextMenuCSS = new ContextMenu();
        contextMenuCSS.getStyleClass().add("context-menu");
        contextMenuCSS.setAutoFix(true);
        contextMenuCSS.setAutoHide(true);
        MenuItem formatCSSOneLineItem = new MenuItem("Format styles in one line");
        formatCSSOneLineItem.setOnAction(e -> beautifyCSS("one_line"));
        contextMenuCSS.getItems().add(formatCSSOneLineItem);

        MenuItem formatCSSMultipleLinesItem = new MenuItem("Format styles in multiple lines");
        formatCSSMultipleLinesItem.setOnAction(e -> beautifyCSS("multiple_lines"));
        contextMenuCSS.getItems().add(formatCSSMultipleLinesItem);
    }

    @PreDestroy
    public void shutdown() {
        logger.info("pre destroy");
        List<Tab> tabs = tabPane.getTabs();
        for (Tab tab : tabs) {
            if (tab.getContent() instanceof CodeEditor) {
                ((CodeEditor)tab.getContent()).shutdown();
            }
        }
    }

    private void writeClipMenuItemChildren(TreeItem<Clip> parentTreeItem, Menu parentMenu) {
        List<TreeItem<Clip>> children = parentTreeItem.getChildren();
        for (TreeItem<Clip> child : children) {
            if (child.getValue().isGroup()) {
                Menu menu = new Menu(child.getValue().getName());
                parentMenu.getItems().add(menu);
                writeClipMenuItemChildren(child, menu);
            }
            else {
                MenuItem menuItem = new MenuItem(child.getValue().getName());
                parentMenu.getItems().add(menuItem);
                menuItem.setOnAction(event -> {
                    insertClip(child.getValue());
                });
            }
        }
    }

    private void insertClip(Clip clip) {
        CodeEditor editor = currentEditor.getValue();
        String selection = editor.getSelection();
        String insertedClip = selection.replaceAll("(?s)^(.*)$", clip.getContent());
        editor.replaceSelection(insertedClip);
        editor.requestFocus();
    }

    private void beautifyCSS(String type) {


    }

    private void openInExternalBrowser(ObjectProperty<CodeEditor> currentEditor) {


    }

    public void openImagePage(ImagePage imagePage) {
        Tab tab = new Tab();
        tab.setClosable(true);
        if (imagePage == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(tabPane.getScene().getWindow());
            alert.setTitle("Image not found");
            alert.getDialogPane().setHeader(null);
            alert.getDialogPane().setHeaderText(null);
            alert.setContentText("The image does not exist and cannot be opened");
            alert.showAndWait();

            return;
        }
        tab.setText(imagePage.getTitle());

        ImageViewerPane pane = new ImageViewerPane();
        pane.setImagePage(imagePage);

        ImageView imageView = pane.getImageView();
        imageView.setImage(imagePage.asNativeFormat());
        imageView.setFitHeight(-1);
        imageView.setFitWidth(-1);

        Label imagePropertiesLabel = pane.getImagePropertiesLabel();
        imagePropertiesLabel.setText(imagePage.getImageDescription());

        tab.setContent(pane);
        tab.setUserData(imagePage);
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);
    }


    private boolean isTabAlreadyOpen(Page page) {
        boolean found = false;
        List<Tab> tabs = tabPane.getTabs();
        for (Tab tab : tabs) {
            if (tab.getUserData().equals(page)) {
                tabPane.getSelectionModel().select(tab);
                found = true;
            }
        }
        return found;
    }

    public void openFileInEditor(Page page) throws IllegalArgumentException {
        openFileInEditor(page, page.getMediaType());
    }

    public void openFileInEditor(Page page, MediaType mediaType) throws IllegalArgumentException {
        if (!isTabAlreadyOpen(page)) {
            configuration.getMainWindow().getScene().setCursor(Cursor.WAIT);
            Tab tab = new Tab();
            tab.setClosable(true);
            if (page == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(tabPane.getScene().getWindow());
                alert.setTitle("File not found");
                alert.getDialogPane().setHeader(null);
                alert.getDialogPane().setHeaderText(null);
                alert.setContentText("The file does not exist and cannot be opened");
                alert.showAndWait();

                return;
            }
            tab.setText(page.getTitle());

            CodeEditor editor;
            if (mediaType.equals(MediaType.CSS)) {
                editor = cssEditorProvider.get();
                editor.setContextMenu(contextMenuCSS);
            }
            else if (mediaType.equals(MediaType.WIKI)) {
                editor = wikiEditorProvider.get();
                editor.setContextMenu(contextMenuArticle);
            }
            else {
                throw new IllegalArgumentException("no editor for mediatype " + mediaType);
            }
            tab.setOnCloseRequest(event -> editor.shutdown());

            tab.setContent((Node) editor);
            tab.setUserData(page);
            tabPane.getTabs().add(tab);
            tabPane.getSelectionModel().select(tab);

            final String code = page.getText();
            editor.stateProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.equals(Worker.State.SUCCEEDED)) {
                    openingEditorTab = true;
                    editor.setCode(code);
                    editor.clearUndoHistory();
                    editor.setCodeEditorSize(((AnchorPane) editor).getWidth() - 20, ((AnchorPane) editor).getHeight() - 20);
                    ((AnchorPane) editor).widthProperty().addListener((observable15, oldValue14, newValue14) -> editor.setCodeEditorSize(newValue14.doubleValue() - 20, ((AnchorPane) editor).getHeight() - 20));
                    ((AnchorPane) editor).heightProperty().addListener((observable12, oldValue1, newValue1) -> editor.setCodeEditorSize(((AnchorPane) editor).getWidth() - 20, newValue1.doubleValue() - 20));
                    editor.setCodeEditorSize(((AnchorPane) editor).getWidth() - 20, ((AnchorPane) editor).getHeight() - 20);
                    ((AnchorPane) editor).widthProperty().addListener((observable13, oldValue12, newValue12) -> editor.setCodeEditorSize(newValue12.doubleValue() - 20, ((AnchorPane) editor).getHeight() - 20));
                    ((AnchorPane) editor).heightProperty().addListener((observable14, oldValue13, newValue13) -> editor.setCodeEditorSize(((AnchorPane) editor).getWidth() - 20, newValue13.doubleValue() - 20));
                    editor.scrollTo(EditorPosition.START);
                    editor.setAbsoluteCursorPosition(0);
                    editor.requestFocus();
                    openingEditorTab = false;
                }
            });

            editor.cursorPositionProperty().addListener((observable, oldValue, newValue) -> {
                EditorPosition cursorPosition = editor.getCursorPosition();
                String textIformation = editor.getTextInformation();
                cursorPosLabelProperty.set("Absolute: " + newValue + ", Relative: " + (cursorPosition.getLine() + 1) + ":" + (cursorPosition.getColumn() + 1)
                        + " | Text Information: " + StringUtils.defaultString(textIformation, ""));
            });

            CodeArea codeArea = editor.getCodeArea();
            codeArea.multiPlainChanges()
                    .successionEnds(java.time.Duration.ofMillis(500))
                    .subscribe(plainTextChanges -> {
                        logger.info("subscribing eventstream");
                        //the openingEditorTab and refreshAllInProgress shows that a code change is in progress, dont reset it here,
                        // the other two that code changes are done, reset that the next changes are executed
                        if (openingEditorTab || refreshAllInProgress || editor.isChangingCode()  || refreshAll) {
                            editor.resetChangingCode();
                            refreshAll = false;
                            return;
                        }
                        CodeEditor codeEditor = currentEditor.getValue();
                        if (codeEditor.getMediaType().equals(MediaType.WIKI)) {
                            currentArticle.get().setText(codeEditor.getCode());
                        }
                        else if (codeEditor.getMediaType().equals(MediaType.CSS)) {
                            currentCssPage.get().setText(codeEditor.getCode());
                        }
                    });

            codeArea.multiPlainChanges()
                    .successionEnds(java.time.Duration.ofMillis(1000))
                    .subscribe(plainTextChanges -> {
                        logger.info("scheduled refresh task, one second after last change");
                        Platform.runLater(() -> {
                            needsRefresh.setValue(true);
                            needsRefresh.setValue(false);
                        });
                    });
            //snychronise caret position with web view
            if (mediaType.equals(MediaType.WIKI)) {
                codeArea.caretPositionProperty().addListener((observable, oldValue, newValue) -> {
                    logger.debug("caret position " + newValue);
                    currentLineProperty.set(newValue);
                });
            }
            configuration.getMainWindow().getScene().setCursor(Cursor.DEFAULT);
        }
    }

    public void closeTab(Page page) {
        List<Tab> tabs =  tabPane.getTabs();
        tabs.stream().filter(tab -> tab.getUserData() == page)
                .findFirst()
                .ifPresent(tab -> tabPane.getTabs().remove(tab));
    }

    public Page getCurrentSearchablePage() {
        return currentSearchablePage.get();
    }

    public ReadOnlyObjectProperty<Page> currentSearchablePageProperty() {
        return currentSearchablePage.getReadOnlyProperty();
    }

    public Article getCurrentArticle() {
        return currentArticle.get();
    }

    public ReadOnlyObjectProperty<Article> currentArticleProperty() {
        return currentArticle.getReadOnlyProperty();
    }

    public CssPage getCurrentCssPage() {
        return currentCssPage.get();
    }

    public ReadOnlyObjectProperty<CssPage> currentCssPageProperty() {
        return currentCssPage.getReadOnlyProperty();
    }

    public BooleanProperty needsRefreshProperty() {
        return needsRefresh;
    }

    public void setTabPane(TabPane tabPane) {
        this.tabPane = tabPane;

        tabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            Page page;
            if (newValue != null && newValue.getContent() instanceof CodeEditor) {
                CodeEditor selectedEditor = (CodeEditor) newValue.getContent();
                page = (Page) newValue.getUserData();
                currentSearchablePage.set(page);
                currentEditor.setValue(selectedEditor);

                if (selectedEditor.getMediaType().equals(MediaType.WIKI) && page instanceof Article) {
                    currentArticle.set((Article) page);
                    currentLineProperty.set(selectedEditor.getCodeArea().getCurrentParagraph());
                }
                else if (selectedEditor.getMediaType().equals(MediaType.CSS) && page instanceof CssPage) {
                    currentCssPage.set((CssPage) page);
                }
                selectedEditor.requestFocus();
            }
        });
    }

    public void scrollTo(EditorPosition position) {
        CodeEditor codeEditor = currentEditor.getValue();
        codeEditor.scrollTo(position);
        codeEditor.requestFocus();
    }

    public void cutSelection() {
        CodeEditor editor = getCurrentEditor();
        editor.getCodeArea().cut();
    }

    public void copySelection() {
        CodeEditor editor = getCurrentEditor();
        editor.getCodeArea().copy();
    }

    public void pasteFromClipboard() {
        CodeEditor editor = getCurrentEditor();
        editor.getCodeArea().paste();
    }


    public void insertAtCursorPositionOrReplaceSelection(String text) {
        if (isInsertablePosition()) {
            CodeEditor editor = getCurrentEditor();
            if(StringUtils.isNotEmpty(editor.getSelection())) {
                editor.replaceSelection(text);
            } else {
                Integer cursorPosition = editor.getAbsoluteCursorPosition();
                editor.insertAt(cursorPosition, text);
            }
            refreshPreview();
            editor.requestFocus();
        }
    }

    /**
     * Inserts the given text at caret position and moves the caret to a new position,
     * the new position is given as difference from the current position
     *
     * @param text text to insert
     * @param moveCaretIndex difference of the current position to that the caret should be moved
     */
    public void insertAtCursorPosition(String text, int moveCaretIndex) {
        CodeEditor codeEditor = currentEditor.getValue();
        if (isInsertablePosition() && codeEditor.getMediaType().equals(MediaType.WIKI)) {
            Integer cursorPosition = codeEditor.getAbsoluteCursorPosition();
            codeEditor.insertAt(cursorPosition, text);
            codeEditor.setAbsoluteCursorPosition(cursorPosition + moveCaretIndex);
            refreshPreview();
            codeEditor.requestFocus();
        }
    }

    public void surroundSelection(String start, String end) {
        CodeEditor codeEditor = currentEditor.getValue();
        if (codeEditor.getMediaType().equals(MediaType.WIKI)) {
            String selection = codeEditor.getSelection();
            codeEditor.replaceSelection(start + selection + end);
            refreshPreview();
            codeEditor.requestFocus();
        }
    }

    public void insertStyle(String styleName, String value) {
        CodeEditor codeEditor = currentEditor.getValue();
        if (codeEditor.getMediaType().equals(MediaType.WIKI)) {
            WikiRichTextCodeEditor xhtmlCodeEditor = (WikiRichTextCodeEditor) codeEditor;
            //xhtmlCodeEditor.insertStyle(styleName, value);
            refreshPreview();
            xhtmlCodeEditor.requestFocus();
        }
    }

    public void surroundSelectionWithTag(String tagName) {
        CodeEditor codeEditor = currentEditor.getValue();
        if (codeEditor.getMediaType().equals(MediaType.WIKI)) {
            String selection = codeEditor.getSelection();
            codeEditor.replaceSelection("<" + tagName + ">" + selection + "</" + tagName + ">");
            refreshPreview();
            codeEditor.requestFocus();
        }
    }


    public void increaseIndent() {
        CodeEditor codeEditor = currentEditor.getValue();
        if (codeEditor.getMediaType().equals(MediaType.WIKI)) {
            WikiRichTextCodeEditor xhtmlCodeEditor = (WikiRichTextCodeEditor) codeEditor;
            xhtmlCodeEditor.increaseIndent();
        }
        codeEditor.requestFocus();
    }

    public void decreaseIndent() {
        CodeEditor codeEditor = currentEditor.getValue();
        if (codeEditor.getMediaType().equals(MediaType.WIKI)) {
            WikiRichTextCodeEditor xhtmlCodeEditor = (WikiRichTextCodeEditor) codeEditor;
            xhtmlCodeEditor.decreaseIndent();
        }
        codeEditor.requestFocus();
    }

    public void toUpperCase() {
        CodeEditor codeEditor = currentEditor.getValue();
        String selectedText = codeEditor.getSelection();
        Locale spellcheckLocale = preferencesManager.getLanguageSpellSelection().getLanguage().getLocaleWithCountryAndVariant();
        String uppercaseText = StringUtils.upperCase(selectedText, spellcheckLocale);
        codeEditor.replaceSelection(uppercaseText);
        codeEditor.requestFocus();
    }

    public void toLowerCase() {
        CodeEditor codeEditor = currentEditor.getValue();
        String selectedText = codeEditor.getSelection();
        Locale spellcheckLocale = preferencesManager.getLanguageSpellSelection().getLanguage().getLocaleWithCountryAndVariant();
        String lowercaseText = StringUtils.lowerCase(selectedText, spellcheckLocale);
        codeEditor.replaceSelection(lowercaseText);
        codeEditor.requestFocus();
    }

    public void refreshPreview() {
        if (currentEditorIsArticle.get()) {
            CodeEditor editor = currentEditor.getValue();
            if (editor != null && currentArticle.get() != null) {
                currentArticle.get().setText(editor.getCode());
            }
        }
        needsRefresh.setValue(true);
        needsRefresh.setValue(false);
    }

    public void reset() {
    }

    public ObservableBooleanValue currentEditorIsArticleProperty() {
        return currentEditorIsArticle;
    }

    public boolean currentEditorIsXHTML() {
        return currentEditorIsArticle.get();
    }

    public boolean getCanRedo() {
        return canRedo.get();
    }

    public SimpleBooleanProperty canRedoProperty() {
        return canRedo;
    }

    public boolean getCanUndo() {
        return canUndo.get();
    }

    public SimpleBooleanProperty canUndoProperty() {
        return canUndo;
    }

    public CodeEditor getCurrentEditor() {
        return currentEditor.getValue();
    }

    public ObjectProperty<CodeEditor> currentEditorProperty() {
        return currentEditor;
    }

    public void refreshAll() {
        //refresh all is in progress, avoid firing listener
        refreshAllInProgress = true;
        //refresh was executed, and after execution avoid firing listener, will be reseted by listener
        refreshAll = true;
        CodeEditor previousCodeEditor = currentEditor.get();
        List<Tab> tabs = tabPane.getTabs();
        for (Tab tab : tabs) {
            Page page = (Page) tab.getUserData();
            if (tab.getContent() instanceof CodeEditor) {
                CodeEditor editor = (CodeEditor) tab.getContent();
                currentEditor.setValue(editor);
                editor.setCode(page.getText());
                editor.scrollTo(0);
            }
        }
        currentEditor.set(previousCodeEditor);
        refreshAllInProgress = false;
    }

    public void refreshEditorCode(Page pageToUpdate) {
        openingEditorTab = true;
        List<Tab> tabs = tabPane.getTabs();
        for (Tab tab : tabs) {
            Page page = (Page) tab.getUserData();
            if (pageToUpdate.equals(page)) {
                CodeEditor editor = (CodeEditor) tab.getContent();
                editor.setCode(pageToUpdate.getText());
                editor.setAbsoluteCursorPosition(0);
            }
        }
        openingEditorTab = false;
    }

    public void refreshImageViewer(Page pageToUpdate) {
        List<Tab> tabs = tabPane.getTabs();
        for (Tab tab : tabs) {
            Page page = (Page) tab.getUserData();
            if (pageToUpdate.equals(page)) {
                ImagePage imageResource = (ImagePage) pageToUpdate;
                logger.info("refreshing image page");
                ImageViewerPane imageViewerPane = (ImageViewerPane) tab.getContent();
                ImageView imageView = imageViewerPane.getImageView();
                imageView.setImage(imageResource.asNativeFormat());
                imageView.setFitHeight(-1);
                imageView.setFitWidth(-1);

                Label imagePropertiesLabel = imageViewerPane.getImagePropertiesLabel();
                imagePropertiesLabel.setText(imageResource.getImageDescription());
            }
        }
    }

    public boolean isInsertablePosition() {
        return true;
    }

    public void scrollTo(Deque<ElementPosition> nodeChain) {
        currentEditor.get().requestFocus();
    }

    public ObservableValue<? extends String> cursorPosLabelProperty() {
        return cursorPosLabelProperty;
    }

    public final ReadOnlyIntegerProperty currentLineProperty() {
        return currentLineProperty.getReadOnlyProperty();
    }
    public final int getCurrentLine() {
        return currentLineProperty.get();
    }
}
