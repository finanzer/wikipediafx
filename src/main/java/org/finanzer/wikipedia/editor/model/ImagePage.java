package org.finanzer.wikipedia.editor.model;

import javafx.scene.image.Image;

public class ImagePage extends Page<Image> {
    private String imageDescription;

    @Override
    public Image asNativeFormat() {
        return null;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }
}
