package org.finanzer.wikipedia.editor.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.geometry.Point2D;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;
import javafx.util.Callback;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import org.finanzer.wikipedia.editor.EditorConfiguration;
import org.finanzer.wikipedia.editor.gui.MainController;
import org.finanzer.wikipedia.editor.javafx.FXUtils;
import org.finanzer.wikipedia.editor.javafx.cells.EditingTreeCell;

import com.google.common.io.Files;
import org.finanzer.wikipedia.editor.model.Article;
import org.finanzer.wikipedia.editor.model.ArticleCollection;
import org.finanzer.wikipedia.editor.model.ArticleCollections;
import org.finanzer.wikipedia.editor.model.CssPage;
import org.finanzer.wikipedia.editor.model.ImagePage;
import org.finanzer.wikipedia.editor.model.MediaType;
import org.finanzer.wikipedia.editor.model.Page;

/**
 * User: mjungierek
 * Date: 23.07.2014
 * Time: 22:17
 */
@Singleton
public class ArticleManager
{
    private static final Logger logger = Logger.getLogger(ArticleManager.class);

    private static final String CSS_FILE_ICON = "/icons/icons8_CSS_Filetype_96px.png";
    private static final String IMAGE_FILE_ICON = "/icons/icons8_Image_File_96px.png";
    private static final String ARTICLE_FILE_ICON = "/icons/icons8_Code_File_96px.png";

    private TreeItem<Page> rootItem;

    private TreeView<Page> treeView;
    private EditorTabManager editorManager;

    @Inject
    private Provider<MainController> mainControllerProvider;

    @Inject
    private EditorConfiguration configuration;

    @Inject
    private ArticleCollections articleCollections;

    private static class FolderSymbolListener implements ChangeListener<Boolean>
    {
        TreeItem<Page> item;

        private FolderSymbolListener(TreeItem<Page> item)
        {
            this.item = item;
        }

        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue)
        {
            if (newValue)
            {
                item.setGraphic(FXUtils.getIcon("/icons/icons8_Open_48px.png", 24));
            }
            else
            {
                item.setGraphic(FXUtils.getIcon("/icons/icons8_Folder_48px.png", 24));
            }
        }
    }

    public class ArticleBrowserTreeCellFactory implements Callback<TreeView<Page>, TreeCell<Page>>
    {
        private final DataFormat OBJECT_DATA_FORMAT = new DataFormat("application/java-object");

        @SuppressWarnings("unchecked")
        @Override
        public TreeCell<Page> call(TreeView<Page> resourceTreeView)
        {
            EditingTreeCell<Page> treeCell = new EditingTreeCell<>(true);

            treeCell.itemProperty().addListener((observable, oldValue, newValue) -> {
                treeCell.setEditable(false);
            });

            treeCell.setOnDragDetected(mouseEvent -> {
                TreeCell<Article> cell = (EditingTreeCell<Article>) mouseEvent.getSource();
                logger.info("dnd detected on item " + cell);
                Page page = cell.getItem();
                if (MediaType.WIKI.equals(page.getMediaType()))
                {
                    Dragboard dragBoard = cell.startDragAndDrop(TransferMode.MOVE);
                    ClipboardContent content = new ClipboardContent();
                    content.put(OBJECT_DATA_FORMAT, page);
                    dragBoard.setContent(content);
                    mouseEvent.consume();
                }
            });

            treeCell.setOnDragDropped(dragEvent -> {
                logger.info("dnd dropped of item " + dragEvent.getSource());
                Object o = dragEvent.getDragboard().getContent(OBJECT_DATA_FORMAT);
                if (o != null)
                {
                    TreeCell<Page> draggedCell = (TreeCell<Page>) dragEvent.getGestureSource();
                    logger.info("dragged cell is " + draggedCell.getItem());
                    logger.info("cell dropped on is " + treeCell.getItem());

                    TreeItem<Page> draggedItem = draggedCell.getTreeItem();
                    TreeItem<Page> parent = draggedItem.getParent();
                    parent.getChildren().remove(draggedItem);
                    int index = parent.getChildren().indexOf(treeCell.getTreeItem());
                    parent.getChildren().add(index, draggedItem);
                    treeView.getSelectionModel().select(draggedItem);

                    treeView.getSelectionModel().clearSelection();
                    treeView.getSelectionModel().select(draggedCell.getTreeItem());
                }
                // remove all dnd effects
                treeCell.setEffect(null);
                treeCell.getStyleClass().remove("dnd-below");

                dragEvent.consume();
            });

            treeCell.setOnDragOver(event -> {
                Page page = treeCell.getItem();
                if (MediaType.WIKI.equals(page.getMediaType()))
                {
                    Point2D sceneCoordinates = treeCell.localToScene(0d, 0d);

                    double height = treeCell.getHeight();

                    // get the y coordinate within the control
                    double y = event.getSceneY() - (sceneCoordinates.getY());

                    // set the dnd effect for the required action
                    if (y > (height * .75d))
                    {
                        treeCell.getStyleClass().add("dnd-below");
                        treeCell.setEffect(null);
                    }
                    else
                    {
                        treeCell.getStyleClass().remove("dnd-below");

                        InnerShadow shadow = new InnerShadow();
                        shadow.setOffsetX(1.0);
                        shadow.setColor(Color.web("#666666"));
                        shadow.setOffsetY(1.0);
                        treeCell.setEffect(shadow);
                    }
                    event.acceptTransferModes(TransferMode.MOVE);
                }
            });

            treeCell.setOnDragExited(event -> {
                // remove all dnd effects
                treeCell.setEffect(null);
                treeCell.getStyleClass().remove("dnd-below");
            });

            return treeCell;
        }
    }

    public void setTreeView(TreeView<Page> initTreeView)
    {
        this.treeView = initTreeView;
        treeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        rootItem = new TreeItem<>();
        rootItem.setExpanded(true);
        treeView.setRoot(rootItem);
        treeView.setShowRoot(false);
        treeView.setEditable(true);

        treeView.setCellFactory(new ArticleBrowserTreeCellFactory());

        treeView.setOnMouseClicked(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY))
            {
                if (event.getClickCount() == 2)
                {
                    TreeItem<Page> item = treeView.getSelectionModel().getSelectedItem();
                    if (item.getValue() instanceof Article)
                    {
                        editorManager.openFileInEditor(item.getValue(), MediaType.WIKI);
                    }
                    else if (item.getValue() instanceof CssPage)
                    {
                        editorManager.openFileInEditor(item.getValue(), MediaType.CSS);
                    }
                    else if (item.getValue() instanceof ImagePage)
                    {
                        editorManager.openImagePage((ImagePage)item.getValue());
                    }
                    event.consume();
                }
            }
            else if (event.getButton().equals(MouseButton.SECONDARY))
            {
                TreeItem<Page> item = treeView.getSelectionModel().getSelectedItem();
                if (item.getValue() instanceof Article)
                {
                    createArticleItemContextMenu(item).show(item.getGraphic(), event.getScreenX(), event.getScreenY());
                }
                else if (item.getValue() instanceof CssPage)
                {
                    createCssItemContextMenu(item).show(item.getGraphic(), event.getScreenX(), event.getScreenY());
                }
                else if (item.getValue() instanceof ImagePage)
                {
                    createImageItemContextMenu(item).show(item.getGraphic(), event.getScreenX(), event.getScreenY());
                }
            }
        });
        
        treeView.setOnEditCommit(event -> {
            logger.info("editing end for new value " + event.getNewValue());
            editorManager.refreshAll();
            editorManager.refreshPreview();
        });

        treeView.setOnKeyPressed(event -> {
            KeyCode keyCode = event.getCode();
            logger.info("key typed in tree view editor: " + keyCode);

            if (keyCode.equals(KeyCode.C) && event.isShortcutDown()) {
                logger.debug("Ctrl-C pressed");
                copyFileNameToClipboard();
            }
        });

    }

    private ContextMenu createArticleItemContextMenu(TreeItem<Page> treeItem)
    {
        ContextMenu menu = new ContextMenu();
        menu.setAutoFix(true);
        menu.setAutoHide(true);

        Menu openWithItem = new Menu("Open with");
        menu.getItems().add(openWithItem);
        addArticleOpenWithApplicationItems(openWithItem, treeItem);

        MenuItem item = new MenuItem("Save as...");
        item.setUserData(treeItem);
        item.setOnAction((ActionEvent event) -> {
            saveAs(treeItem);
        });
        menu.getItems().add(item);

        return menu;
    }

    private void addArticleOpenWithApplicationItems(Menu menu, TreeItem<Page> treeItem)
    {
        List<EditorConfiguration.OpenWithApplication> applications = configuration.getXhtmlOpenWithApplications();
        for (EditorConfiguration.OpenWithApplication application : applications)
        {
            MenuItem item = new MenuItem(application.getDisplayName());
            item.setUserData(treeItem);
            item.setOnAction(event -> openWithApplication(treeItem, application.getFileName()));
            menu.getItems().add(item);
        }
        MenuItem item = new MenuItem("Configure Application...");
        item.setUserData(treeItem);
        item.setOnAction(event -> configureApplicationForOpenXHTML(treeItem));
        menu.getItems().add(item);
    }

    private ContextMenu createCssItemContextMenu(TreeItem<Page> treeItem)
    {
        ContextMenu menu = new ContextMenu();
        menu.setAutoFix(true);
        menu.setAutoHide(true);

        MenuItem item = new MenuItem("Validate");
        item.setUserData(treeItem);
        item.setOnAction(event -> validateCss(treeItem));
        menu.getItems().add(item);

        item = new SeparatorMenuItem();
        menu.getItems().add(item);

        Menu openWithItem = new Menu("Open with");
        menu.getItems().add(openWithItem);
        addCssOpenWithApplicationItems(openWithItem, treeItem);

        item = new MenuItem("Save as...");
        item.setUserData(treeItem);
        item.setOnAction(event -> saveAs(treeItem));
        menu.getItems().add(item);

        return menu;
    }

    private void addCssOpenWithApplicationItems(Menu menu, TreeItem<Page> treeItem)
    {
        List<EditorConfiguration.OpenWithApplication> applications = configuration.getCssOpenWithApplications();
        for (EditorConfiguration.OpenWithApplication application : applications)
        {
            MenuItem item = new MenuItem(application.getDisplayName());
            item.setUserData(treeItem);
            item.setOnAction(event -> openWithApplication(treeItem, application.getFileName()));
            menu.getItems().add(item);
        }
        MenuItem item = new MenuItem("Configure Application...");
        item.setUserData(treeItem);
        item.setOnAction(event -> configureApplicationForOpenCSS(treeItem));
        menu.getItems().add(item);
    }

    private ContextMenu createImageItemContextMenu(TreeItem<Page> treeItem)
    {
        ContextMenu menu = new ContextMenu();
        menu.setAutoFix(true);
        menu.setAutoHide(true);

        Menu openWithItem = new Menu("Open with");
        menu.getItems().add(openWithItem);
        addImageOpenWithApplicationItems(openWithItem, treeItem);

        MenuItem item = new MenuItem("Save as...");
        item.setUserData(treeItem);
        item.setOnAction(event -> saveAs(treeItem));
        menu.getItems().add(item);

        return menu;
    }

    private void addImageOpenWithApplicationItems(Menu menu, TreeItem<Page> treeItem)
    {
        List<EditorConfiguration.OpenWithApplication> applications = configuration.getImageOpenWithApplications();
        for (EditorConfiguration.OpenWithApplication application : applications)
        {
            MenuItem item = new MenuItem(application.getDisplayName());
            item.setUserData(treeItem);
            item.setOnAction(event -> openWithApplication(treeItem, application.getFileName()));
            menu.getItems().add(item);
        }
        MenuItem item = new MenuItem("Configure Application ...");
        item.setUserData(treeItem);
        item.setOnAction(event -> configureApplicationForOpenImage(treeItem));
        menu.getItems().add(item);
    }

    public void refreshBookBrowser()
    {
        rootItem.getChildren().clear();

        for (ArticleCollection articleCollection : articleCollections.getCollections()) {
            Page collection = new Page(articleCollection.getName()) {
                @Override
                public Object asNativeFormat() {
                    return null;
                }
            };
            TreeItem<Page> collectionItem = new TreeItem<>(collection);
            collectionItem.setGraphic(FXUtils.getIcon("/icons/icons8_Folder_48px.png", 24));
            collectionItem.expandedProperty().addListener(new FolderSymbolListener(collectionItem));
            rootItem.getChildren().add(collectionItem);

        }

        treeView.getSelectionModel().clearSelection();
    }

    public void setEditorManager(EditorTabManager editorManager)
    {
        this.editorManager = editorManager;
    }

    private void copyFileNameToClipboard() {
        TreeItem<Page> treeItem = treeView.getSelectionModel().getSelectedItem();
        Clipboard.getSystemClipboard().setContent(Collections.singletonMap(DataFormat.PLAIN_TEXT, treeItem.getValue().getTitle()));
    }

    private void saveAs(TreeItem<Page> treeItem)
    {


    }

    private void openWithApplication(TreeItem<Page> treeItem, String applicationExecutable)
    {
        Page page = treeItem.getValue();
        try
        {
            File tmp = new File(Files.createTempDir(), page.getTitle() + ".wiki.txt");
            FileOutputStream output = new FileOutputStream(tmp);
            output.write(page.getText().getBytes(StandardCharsets.UTF_8));
            output.flush();
            output.close();

            Runtime.getRuntime().exec(applicationExecutable + " " + tmp);
            WatchService watcher = FileSystems.getDefault().newWatchService();
            Path path = tmp.toPath().getParent();
            path.register(watcher, StandardWatchEventKinds.ENTRY_MODIFY);
            Task task = new Task<Void>()
            {
                @Override
                public Void call()
                {
                    for (;;)
                    {
                        // wait for key to be signaled
                        WatchKey key;
                        try
                        {
                            key = watcher.take();
                        }
                        catch (InterruptedException x)
                        {
                            return null;
                        }

                        for (WatchEvent<?> event : key.pollEvents())
                        {
                            WatchEvent.Kind<?> kind = event.kind();

                            // This key is registered only
                            // for ENTRY_CREATE events,
                            // but an OVERFLOW event can
                            // occur regardless if events
                            // are lost or discarded.
                            if (kind == StandardWatchEventKinds.ENTRY_MODIFY)
                            {
                                WatchEvent<Path> ev = (WatchEvent<Path>)event;
                                Path filename = ev.context();
                                logger.info("getting modify event for file " + filename);
                                Platform.runLater(() -> {
                                    try
                                    {
                                        byte[] data = IOUtils.toByteArray(new FileInputStream(tmp));
                                        if (data.length == 0)
                                        {
                                            logger.info("file " + filename + " looks like its writing from external application, ignore this event");
                                            return;
                                        }
                                        page.setText(StringUtils.toEncodedString(data, StandardCharsets.UTF_8));
                                        if (page instanceof Article || page instanceof CssPage)
                                        {
                                            editorManager.refreshEditorCode(page);
                                        }
                                        else
                                        {
                                            editorManager.refreshImageViewer(page);
                                        }
                                    }
                                    catch (IOException e)
                                    {
                                        logger.error("error while reading content written by external program", e);
                                    }
                                });

                            }
                        }

                        // Reset the key -- this step is critical if you want to
                        // receive further watch events.  If the key is no longer valid,
                        // the directory is inaccessible so exit the loop.
                        boolean valid = key.reset();
                        if (!valid)
                        {
                            logger.info("end watching for modified file ");
                            break;
                        }
                    }
                    try
                    {
                        watcher.close();
                    }
                    catch (IOException e)
                    {
                        logger.error("error while closing watcher service", e);
                    }
                    return null;
                }
            };
            new Thread(task).start();
        }
        catch (IOException e)
        {
            logger.error("error while opening file in external program", e);
        }
    }

    private void renameItem(TreeItem<Page> treeItem)
    {
        ((EditingTreeCell)treeItem.getGraphic().getParent()).startEdit();
    }


    private void selectAll(TreeItem<Page> parentItem)
    {
        List<TreeItem<Page>> treeItems = parentItem.getChildren();
        int[] indices = new int[treeItems.size()];
        int i = 0;
        for (TreeItem<Page> item : treeItems)
        {
            indices[i] = treeView.getRow(item);
            i++;
        }
        treeView.getSelectionModel().selectIndices(indices[0], indices);
    }

    private void validateCss(TreeItem<Page> treeItem)
    {


    }

    private void configureApplicationForOpenXHTML(TreeItem<Page> treeItem)
    {


    }

    private void configureApplicationForOpenCSS(TreeItem<Page> treeItem)
    {


    }

    private void configureApplicationForOpenImage(TreeItem<Page> treeItem)
    {


    }

}
