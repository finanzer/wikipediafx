package org.finanzer.wikipedia.editor.gui;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.inject.Inject;
import javax.inject.Singleton;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import org.finanzer.wikipedia.editor.BeanFactory;
import org.finanzer.wikipedia.editor.EditorConfiguration;
import org.finanzer.wikipedia.editor.clips.Clip;
import org.finanzer.wikipedia.editor.editor.CodeEditor;

import org.finanzer.wikipedia.editor.javafx.StashableSplitPane;
import org.finanzer.wikipedia.editor.model.ArticleCollections;
import org.finanzer.wikipedia.editor.model.ArticleSection;
import org.finanzer.wikipedia.editor.model.Page;
import org.jdom2.Document;

import org.finanzer.wikipedia.editor.manager.ArticleManager;
import org.finanzer.wikipedia.editor.manager.EditorTabManager;
import org.finanzer.wikipedia.editor.manager.PreviewManager;
import org.finanzer.wikipedia.editor.manager.SearchManager;
import org.finanzer.wikipedia.editor.manager.TOCViewManager;
import org.finanzer.wikipedia.editor.preferences.PreferencesLanguageStorable;
import org.finanzer.wikipedia.editor.preferences.PreferencesManager;
import org.finanzer.wikipedia.editor.preferences.QuotationMark;
import org.finanzer.wikipedia.editor.preferences.StartupType;

/**
 * User: mjungierek
 * Date: 07.02.14
 * Time: 19:09
 */
@Singleton
public class MainController implements Initializable
{
    private static final Logger logger = Logger.getLogger(MainController.class);
    @FXML
    private Button halfCharacterButton;
    @FXML
    private Button quarterCharacterButton;
    @FXML
    private Button threeQuarterCharacterButton;
    @FXML
    private Label previewWidthLabel;
    @FXML
    private Button nonBreakingSpaceButton;
    @FXML
    private Button hrButton;
    @FXML
    private Button ellipsisButton;
    @FXML
    private Button blockQuoteButton;
    @FXML
    private Button singleQuotationMarksButton;
    @FXML
    private SplitPane mainDivider;
    @FXML
    private StashableSplitPane centerDivider;
    @FXML
    private StashableSplitPane rightDivider;
    @FXML
    private AnchorPane previewAnchorPane;
    @FXML
    private ToggleButton showBookBrowserToggleButton;
    @FXML
    private ToggleButton showPreviewToggleButton;
    @FXML
    private ToggleButton showTocToggleButton;
    @FXML
    private Button insertTableButton;
    @FXML
    private StashableSplitPane leftDivider;
    @FXML
    private ToggleButton showClipsToggleButton;
    @FXML
    private Button zoomInButton;
    @FXML
    private Button zoom100Button;
    @FXML
    private Button zoomOutButton;
    @FXML
    private Button increaseIndentButton;
    @FXML
    private Button decreaseIndentButton;
    @FXML
    private Button insertSpecialCharacterButton;
    @FXML
    private Label cursorPosLabel;
    @FXML
    private Button saveAsButton;
    @FXML
    private Button insertImageButton;
    @FXML
    private Button splitButton;
    @FXML
    private Button h1Button;
    @FXML
    private Button h2Button;
    @FXML
    private Button h3Button;
    @FXML
    private Button h4Button;
    @FXML
    private Button h5Button;
    @FXML
    private Button h6Button;
    @FXML
    private Button paragraphButton;
    @FXML
    private Button quotationMarksButton;
    @FXML
    private Button undoButton;
    @FXML
    private Button redoButton;
    @FXML
    private Button cutButton;
    @FXML
    private Button copyButton;
    @FXML
    private Button pasteButton;
    @FXML
    private ToggleButton searchReplaceButton;
    @FXML
    private MenuButton openPageButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button alignLeftButton;
    @FXML
    private Button centerButton;
    @FXML
    private Button rightAlignButton;
    @FXML
    private Button justifyButton;
    @FXML
    private Button orderedListButton;
    @FXML
    private Button unorderedListButton;
    @FXML
    private Button underlineButton;
    @FXML
    private Button strikeButton;
    @FXML
    private Button subscriptButton;
    @FXML
    private Button superscriptButton;
    @FXML
    private Button boldButton;
    @FXML
    private Button italicButton;
    @FXML
    private ListView<Clip> clipListView;
    @FXML
    private TreeView<ArticleSection> tocTreeView;
    @FXML
    private WebView previewWebview;
    @FXML
    private TabPane pagesTabPane;
    @FXML
    private AnchorPane statusAnchorPane;
    @FXML
    private TreeView<Page> epubStructureTreeView;
    @FXML
    private Button insertLinkButton;
    @FXML
    private AnchorPane centerAnchorPane;
    @FXML
    private Button uppercaseButton;
    @FXML
    private Button lowercaseButton;
    @FXML
    private ComboBox<PreferencesLanguageStorable> languageSpellComboBox;

    private List<MenuItem> recentPagesMenuItems = new ArrayList<>();
    private Stage stage;

    @Inject
    private ArticleManager articleManager;
    @Inject
    private EditorTabManager editorTabManager;
    @Inject
    private PreviewManager previewManager;
    @Inject
    private TOCViewManager tocViewManager;
    @Inject
    private EditorConfiguration configuration;
    @Inject
    private SearchManager searchManager;
    @Inject
    private SearchAnchorPane searchAnchorPane;
    @Inject
    private PreferencesManager preferencesManager;
    @Inject
    private ArticleCollections articleCollections;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        articleManager.setTreeView(epubStructureTreeView);
        articleManager.setEditorManager(editorTabManager);

        pagesTabPane.getTabs().clear();
        pagesTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.ALL_TABS);
        editorTabManager.setTabPane(pagesTabPane);

        previewManager.setWebview(previewWebview);

        tocViewManager.setTreeView(tocTreeView);
        tocViewManager.setEditorManager(editorTabManager);

        BooleanBinding isNoArticleEditorBinding = Bindings.not(editorTabManager.currentEditorIsArticleProperty())
                .or(Bindings.isEmpty(pagesTabPane.getTabs()));
        BooleanBinding isNoEditorBinding = Bindings.isEmpty(pagesTabPane.getTabs())
                .or(editorTabManager.currentArticleProperty().isNull())
                .and(editorTabManager.currentCssPageProperty().isNull())
                .and(editorTabManager.currentArticleProperty().isNull());

        h1Button.disableProperty().bind(isNoArticleEditorBinding);
        h2Button.disableProperty().bind(isNoArticleEditorBinding);
        h3Button.disableProperty().bind(isNoArticleEditorBinding);
        h4Button.disableProperty().bind(isNoArticleEditorBinding);
        h5Button.disableProperty().bind(isNoArticleEditorBinding);
        h6Button.disableProperty().bind(isNoArticleEditorBinding);
        paragraphButton.disableProperty().bind(isNoArticleEditorBinding);
        quotationMarksButton.disableProperty().bind(isNoArticleEditorBinding);
        singleQuotationMarksButton.disableProperty().bind(isNoArticleEditorBinding);
        blockQuoteButton.disableProperty().bind(isNoArticleEditorBinding);

        boldButton.disableProperty().bind(isNoArticleEditorBinding);
        italicButton.disableProperty().bind(isNoArticleEditorBinding);
        orderedListButton.disableProperty().bind(isNoArticleEditorBinding);
        unorderedListButton.disableProperty().bind(isNoArticleEditorBinding);
        underlineButton.disableProperty().bind(isNoArticleEditorBinding);
        strikeButton.disableProperty().bind(isNoArticleEditorBinding);
        subscriptButton.disableProperty().bind(isNoArticleEditorBinding);
        superscriptButton.disableProperty().bind(isNoArticleEditorBinding);
        alignLeftButton.disableProperty().bind(isNoArticleEditorBinding);
        centerButton.disableProperty().bind(isNoArticleEditorBinding);
        rightAlignButton.disableProperty().bind(isNoArticleEditorBinding);
        justifyButton.disableProperty().bind(isNoArticleEditorBinding);
        ellipsisButton.disableProperty().bind(isNoArticleEditorBinding);
        nonBreakingSpaceButton.disableProperty().bind(isNoArticleEditorBinding);
        hrButton.disableProperty().bind(isNoArticleEditorBinding);
        halfCharacterButton.disableProperty().bind(isNoArticleEditorBinding);
        quarterCharacterButton.disableProperty().bind(isNoArticleEditorBinding);
        threeQuarterCharacterButton.disableProperty().bind(isNoArticleEditorBinding);

        saveButton.setDisable(true);
        undoButton.disableProperty().bind(isNoEditorBinding.or(Bindings.not(editorTabManager.canUndoProperty())));
        redoButton.disableProperty().bind(isNoEditorBinding.or(Bindings.not(editorTabManager.canRedoProperty())));

        cutButton.disableProperty().bind(isNoEditorBinding);
        copyButton.disableProperty().bind(isNoEditorBinding);
        pasteButton.disableProperty().bind(isNoEditorBinding);

        searchReplaceButton.disableProperty().bind(isNoEditorBinding);

        increaseIndentButton.disableProperty().bind(isNoArticleEditorBinding);
        decreaseIndentButton.disableProperty().bind(isNoArticleEditorBinding);
        insertImageButton.disableProperty().bind(isNoArticleEditorBinding);
        splitButton.disableProperty().bind(isNoArticleEditorBinding);
        insertTableButton.disableProperty().bind(isNoArticleEditorBinding);
        insertSpecialCharacterButton.disableProperty().bind(isNoArticleEditorBinding);
        insertLinkButton.disableProperty().bind(isNoArticleEditorBinding);
        lowercaseButton.disableProperty().bind(isNoEditorBinding);
        uppercaseButton.disableProperty().bind(isNoEditorBinding);

        cursorPosLabel.textProperty().bind(editorTabManager.cursorPosLabelProperty());
        previewWidthLabel.textProperty().bind(Bindings.createStringBinding(() -> "Preview width: " + previewWebview.widthProperty().getValue(), previewWebview.widthProperty()));

        //Teile der Oberfläche an-/abschalten, per Binding an die Buttons im Ribbon
        clipListView.visibleProperty().bindBidirectional(showClipsToggleButton.selectedProperty());
        showClipsToggleButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
            {
                if (!leftDivider.getItems().contains(clipListView))
                {
                    leftDivider.getItems().add(clipListView);
                    leftDivider.setDividerPosition(0, 0.7);
                }
            }
            leftDivider.setVisibility(1, newValue);
        });
        epubStructureTreeView.visibleProperty().bindBidirectional(showBookBrowserToggleButton.selectedProperty());
        showBookBrowserToggleButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
            {
                if (!leftDivider.getItems().contains(epubStructureTreeView))
                {
                    leftDivider.getItems().add(epubStructureTreeView);
                    leftDivider.setDividerPosition(0, 0.7);
                }
            }
            leftDivider.setVisibility(0, newValue);
        });

        previewAnchorPane.visibleProperty().bindBidirectional(showPreviewToggleButton.selectedProperty());
        showPreviewToggleButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
            {
                if (!rightDivider.getItems().contains(previewAnchorPane))
                {
                    rightDivider.getItems().add(previewAnchorPane);
                    rightDivider.setDividerPosition(0, 0.7);
                }
            }
            rightDivider.setVisibility(0, newValue);
        });
        tocTreeView.visibleProperty().bindBidirectional(showTocToggleButton.selectedProperty());
        showTocToggleButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
            {
                if (!rightDivider.getItems().contains(tocTreeView))
                {
                    rightDivider.getItems().add(tocTreeView);
                    rightDivider.setDividerPosition(0, 0.7);
                }
            }
            rightDivider.setVisibility(1, newValue);
        });

        ObservableList<String> recentPages = configuration.getRecentPages();
        recentPages.addListener((ListChangeListener<String>) change -> {
            ObservableList<String> currentRecentPages = configuration.getRecentPages();
            createRecentFilesMenuItems(currentRecentPages);
        });

        centerAnchorPane.getChildren().add(searchAnchorPane);
        AnchorPane.setTopAnchor(searchAnchorPane, 0.0);
        AnchorPane.setLeftAnchor(searchAnchorPane, 0.0);
        AnchorPane.setRightAnchor(searchAnchorPane, 0.0);
        searchAnchorPane.visibleProperty().bind(isNoEditorBinding.not().and(searchReplaceButton.selectedProperty()));
        searchAnchorPane.visibleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue)
            {
                AnchorPane.setTopAnchor(pagesTabPane, 70.0);
            }
            else
            {
                AnchorPane.setTopAnchor(pagesTabPane, 0.0);
            }
        });
        searchAnchorPane.getCloseButton().setOnAction(event -> {
            searchReplaceButton.setSelected(false);
        });

        languageSpellComboBox.setItems(preferencesManager.getLanguageSpellItems());
        //not bind bidiretional, because selectionModel has only read only properties and preferences can not set values from preferences store if property is bind
        preferencesManager.languageSpellSelectionProperty().addListener((observable, oldValue, newValue) -> languageSpellComboBox.getSelectionModel().select(newValue));
        //initialize the value in combobox
        languageSpellComboBox.getSelectionModel().select(preferencesManager.languageSpellSelectionProperty().get());
        languageSpellComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> preferencesManager.languageSpellSelectionProperty().set(newValue));

        languageSpellComboBox.disableProperty().bind(preferencesManager.spellcheckProperty().not());
    }

    public void initBook() {
        List<String> recentFiles = configuration.getRecentPages();
        if (preferencesManager.getStartupType() == StartupType.RECENT_EBOOK && !recentFiles.isEmpty()) {
            openPage(recentFiles.get(0));
        }
    }


    private void createRecentFilesMenuItems(ObservableList<String> recentFiles)
    {
        openPageButton.getItems().clear();
        int number = 0;
        for (String recentPage : recentFiles)
        {
            if (number > EditorConfiguration.RECENT_FILE_NUMBER)
            {
                break;
            }
            MenuItem recentFileMenuItem = new MenuItem(recentPage);
            recentFileMenuItem.setOnAction(event -> {
                openPage(recentPage);
            });
            openPageButton.getItems().add(recentFileMenuItem);
            recentPagesMenuItems.add(recentFileMenuItem);
            number++;
        }
    }

    public void setStage(Stage stage)
    {
        this.stage = stage;
        stage.setTitle("SmoekerSchriever - EpubFX");

        stage.setOnCloseRequest(event -> {
            checkBeforeClose();
            editorTabManager.shutdown();
        });

        stage.getScene().setOnKeyPressed(event -> {
            if ((event.isControlDown() || event.isShortcutDown()) && event.getCode().equals(KeyCode.F)) {
                logger.debug("Ctrl-F Pressed");
                searchReplaceButton.setSelected(true);
                searchReplaceButtonAction();
            } else if (event.getCode().equals(KeyCode.F3)) {
                searchAnchorPane.findNextAction();
            }
        });

        stage.getScene().getAccelerators().put(new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN), this::openPage);
        stage.getScene().getAccelerators().put(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN), this::savePage);
        stage.getScene().getAccelerators().put(new KeyCodeCombination(KeyCode.I, KeyCombination.SHORTCUT_DOWN), this::italicButtonAction);
    }

    private void openPage() {
        Platform.runLater(() -> {
            stage.getScene().setCursor(Cursor.WAIT);
            //show page chooser
            //editorTabManager.openFileInEditor(page);
        });
    }

    private void openPage(String page) {
        Platform.runLater(() -> {
            stage.getScene().setCursor(Cursor.WAIT);
            //editorTabManager.openFileInEditor(page);
        });
    }

    public void savePage()
    {
    }

    public void exitAction(ActionEvent actionEvent)
    {
        stage.close();
    }

    public void checkBeforeClose()
    {
        if (articleCollections.isUnsavedPage())
        {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.initOwner(stage);
            alert.setTitle("ebook is changed");
            alert.getDialogPane().setHeader(null);
            alert.getDialogPane().setHeaderText(null);
            alert.setContentText("The ebook was changed, save the changes before?");
            alert.getDialogPane().getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> choosedButton = alert.showAndWait();
            choosedButton.ifPresent(buttonType -> {
                if (buttonType.equals(ButtonType.YES)) {
                    savePage();
                }
            });
        }
    }

    public void h1ButtonAction(ActionEvent actionEvent)
    {
        editorTabManager.surroundSelection("=", "=");;
    }

    public void h2ButtonAction(ActionEvent actionEvent)
    {
        editorTabManager.surroundSelection("==", "==");;
    }

    public void h3ButtonAction(ActionEvent actionEvent)
    {
        editorTabManager.surroundSelection("===", "===");;
    }

    public void h4ButtonAction(ActionEvent actionEvent)
    {
        editorTabManager.surroundSelection("====", "====");;
    }

    public void h5ButtonAction(ActionEvent actionEvent)
    {
        editorTabManager.surroundSelection("=====", "=====");;
    }

    public void h6ButtonAction(ActionEvent actionEvent)
    {
        editorTabManager.surroundSelection("======", "======");;
    }

    public void paragraphButtonAction()
    {
    }

    public void quotationMarksButtonAction()
    {
        String selectedQuotationMark = preferencesManager.getQuotationMarkSelection();
        logger.info("select quotation mark " + selectedQuotationMark);
        QuotationMark quotationMark = QuotationMark.findByDescription(selectedQuotationMark);
        editorTabManager.surroundSelection(quotationMark.getLeft(), quotationMark.getRight());
    }

    public void singleQuotationMarksButtonAction()
    {
        String selectedQuotationMark = preferencesManager.getQuotationMarkSelection();
        logger.info("select quotation mark " + selectedQuotationMark);
        QuotationMark quotationMark = QuotationMark.findByDescription(selectedQuotationMark);
        editorTabManager.surroundSelection(quotationMark.getSingleLeft(), quotationMark.getSingleRight());
    }

    public void blockQuoteButtonAction() {
    }

    public void boldButtonAction()
    {
        editorTabManager.surroundSelection("'''''", "'''''");;
    }

    public void italicButtonAction()
    {
        editorTabManager.surroundSelection("''''", "''''");;
    }

    public void nonBreakingSpaceButtonAction() {
        editorTabManager.insertAtCursorPositionOrReplaceSelection("&nbsp;");
    }

    public void hrButtonAction() {
        editorTabManager.insertAtCursorPositionOrReplaceSelection("----");
    }

    public void ellipsisButtonAction() {
        editorTabManager.insertAtCursorPositionOrReplaceSelection("…");
    }

    public void halfCharacterButtonAction() {
        editorTabManager.insertAtCursorPositionOrReplaceSelection("½");
    }


    public void quarterCharacterButtonAction() {
        editorTabManager.insertAtCursorPositionOrReplaceSelection("¼");
    }

    public void threeQuarterCharacterButtonAction() {
        editorTabManager.insertAtCursorPositionOrReplaceSelection("¾");
    }


    public void orderedListButtonAction()
    {
        insertList("ol");
    }

    public void unorderedListButtonAction() {
        insertList("ul");
    }

    private void insertList(String tagName) {
        String tab;
        int tabOffset;
        if (preferencesManager.isUseTab()) {
            tab = "\t";
            tabOffset = 1;
        } else {
            tab = StringUtils.repeat(" ", preferencesManager.getTabSize());
            tabOffset = preferencesManager.getTabSize();
        }

        String olString = "<" + tagName + ">\n" + tab + "<li></li>\n</" + tagName + ">";
        editorTabManager.insertAtCursorPosition(olString, 9 + tabOffset);
    }

    public void underlineButtonAction()
    {
    }

    public void strikeButtonAction()
    {
        editorTabManager.surroundSelectionWithTag("s");
    }

    public void subscriptButtonAction()
    {
        editorTabManager.surroundSelectionWithTag("sub");
    }

    public void superscriptButtonAction()
    {
        editorTabManager.surroundSelectionWithTag("sup");
    }

    public void alignLeftButtonAction()
    {
        editorTabManager.insertStyle("text-align", "left");
    }

    public void centerButtonAction()
    {
        editorTabManager.insertStyle("text-align", "center");
    }

    public void rightAlignButtonAction()
    {
        editorTabManager.insertStyle("text-align", "right");
    }

    public void justifyButtonAction()
    {
        editorTabManager.insertStyle("text-align", "justify");
    }

    public void undoButtonAction()
    {
        CodeEditor currentEditor = editorTabManager.currentEditorProperty().get();
        if (currentEditor != null)
        {
            currentEditor.undo();
        }
    }

    public void redoButtonAction()
    {
        CodeEditor currentEditor = editorTabManager.currentEditorProperty().get();
        if (currentEditor != null)
        {
            currentEditor.redo();
        }
    }

    public void cutButtonAction()
    {
        editorTabManager.cutSelection();
    }

    public void copyButtonAction()
    {
        editorTabManager.copySelection();
        //only copying text is not a change of the book
    }

    public void pasteButtonAction()
    {
        editorTabManager.pasteFromClipboard();
    }

    public void searchReplaceButtonAction()
    {
        //set search string also if pane is already open
        String selection = editorTabManager.getCurrentEditor().getSelection();
        searchAnchorPane.setSearchString(selection);
    }

    public void insertImageButtonAction()
    {
        if (editorTabManager.isInsertablePosition())
        {
            //createAndOpenStandardController("/insert_media.fxml", InsertMediaController.class);
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Einfügen nicht möglich");
            alert.getDialogPane().setHeader(null);
            alert.getDialogPane().setHeaderText(null);
            alert.setContentText("Kann Bild bzw. Mediendatei nicht an dieser Position einfügen. Dies ist nur innerhalb des XHTML-Bodys möglich.");
            alert.showAndWait();
        }
    }

    public void clipEditorAction()
    {
        createAndOpenStandardController("/clip_editor.fxml", ClipEditorController.class);
    }


    public void insertSpecialCharacterAction()
    {
    }

    public void createTocAction()
    {
        Stage stage = createStandardController("/create_toc.fxml", GenerateTocController.class);
        GenerateTocController controller = GenerateTocController.getInstance();
        controller.setEditMode(false);
        stage.show();
    }

    public void editTocAction()
    {
        Stage stage = createStandardController("/create_toc.fxml", GenerateTocController.class);
        GenerateTocController controller = GenerateTocController.getInstance();
        controller.setEditMode(true);
        stage.show();
    }


    public void increaseIndentButtonAction()
    {
        editorTabManager.increaseIndent();
    }

    public void decreaseIndentButtonAction()
    {
        editorTabManager.decreaseIndent();
    }

    public void insertTableButtonAction()
    {
        if (editorTabManager.isInsertablePosition())
        {
            createAndOpenStandardController("/insert-table.fxml", InsertTableController.class);
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Insert not possible");
            alert.getDialogPane().setHeader(null);
            alert.getDialogPane().setHeaderText(null);
            alert.setContentText("Can't insert table on this position. This is only available within the body of the document.");
            alert.showAndWait();
        }
    }

    private void createAndOpenStandardController(String fxmlFile, Class<? extends StandardController> controllerClass)
    {
        Stage windowStage = createStandardController(fxmlFile, controllerClass);
        windowStage.show();
    }

    public Stage createStandardController(String fxmlFile, Class<? extends StandardController> controllerClass)
    {
        Method staticMethod;
        StandardController controller;
        try
        {
            staticMethod = controllerClass.getMethod("getInstance");
            controller = (StandardController) staticMethod.invoke(controllerClass);
        }
        catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e)
        {
            logger.error("", e);
            return null;
        }

        Stage windowStage = null;
        if (controller == null)
        {
            try
            {
                windowStage = new Stage(StageStyle.UTILITY);

                FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlFile), null, new JavaFXBuilderFactory(),
                        type -> BeanFactory.getInstance().getBean(type));

                Pane root = loader.load();
                Scene scene = new Scene(root);
                windowStage.setScene(scene);
                windowStage.initOwner(stage);
                windowStage.initModality(Modality.APPLICATION_MODAL);

                controller = (StandardController) staticMethod.invoke(controllerClass);
                controller.setStage(windowStage);
            }
            catch (IOException | IllegalAccessException | InvocationTargetException e)
            {
                logger.error("", e);
            }
        }
        else
        {
            windowStage = controller.getStage();
        }
        return windowStage;
    }

    public void previewZoomIn()
    {
        double oldZoom = previewWebview.getZoom();
        previewWebview.setZoom(oldZoom + 0.1);
    }

    public void preview100PercentZoom()
    {
        previewWebview.setZoom(1.0);
    }

    public void previewZoomOut()
    {
        double oldZoom = previewWebview.getZoom();
        previewWebview.setZoom(oldZoom - 0.1);
    }

    public ToggleButton getShowBookBrowserToggleButton()
    {
        return showBookBrowserToggleButton;
    }

    public ToggleButton getShowPreviewToggleButton()
    {
        return showPreviewToggleButton;
    }

    public ToggleButton getShowTocToggleButton()
    {
        return showTocToggleButton;
    }

    public ToggleButton getShowClipsToggleButton()
    {
        return showClipsToggleButton;
    }

    public SplitPane getMainDivider()
    {
        return mainDivider;
    }

    public StashableSplitPane getCenterDivider()
    {
        return centerDivider;
    }

    public StashableSplitPane getRightDivider()
    {
        return rightDivider;
    }

    public StashableSplitPane getLeftDivider()
    {
        return leftDivider;
    }

    public void insertLinkAction()
    {
        if (editorTabManager.isInsertablePosition())
        {
            createAndOpenStandardController("/insert-link.fxml", InsertLinkController.class);
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Insert not possible");
            alert.getDialogPane().setHeader(null);
            alert.getDialogPane().setHeaderText(null);
            alert.setContentText("Can't insert link on this position. This is only available within the body of the document.");
            alert.showAndWait();
        }
    }

    public void uppercaseButtonAction() {
        editorTabManager.toUpperCase();
    }

    public void lowercaseButtonAction() {
        editorTabManager.toLowerCase();
    }

    public void settingsButtonAction()
    {
        preferencesManager.showPreferencesDialog();
    }

    public void checkLinksButton() {
    }
}
