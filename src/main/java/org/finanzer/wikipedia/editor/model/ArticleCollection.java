package org.finanzer.wikipedia.editor.model;

import java.util.ArrayList;
import java.util.List;

public class ArticleCollection {
    private String name;
    private List<Article> articles = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Article> getArticles() {
        return articles;
    }
}

