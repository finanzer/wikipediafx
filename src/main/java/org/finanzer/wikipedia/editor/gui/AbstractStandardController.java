package org.finanzer.wikipedia.editor.gui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.stage.Stage;


/**
 * User: Michail Jungierek
 * Date: 13.07.2019
 * Time: 11:25
 */
public abstract class AbstractStandardController implements StandardController {

    protected Stage stage;

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public Stage getStage() {
        return stage;
    }

    public void initialize(URL location, ResourceBundle resources) {
    }
}
